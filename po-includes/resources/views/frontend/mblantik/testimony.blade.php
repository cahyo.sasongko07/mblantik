@extends(getTheme('layouts.app'))

@section('content')
    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials section-bg mt-5">
        <div class="container">

            <div class="section-title">
                <span>Testimonials</span>
                <h2>Testimonials</h2>
                <!-- <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p> -->
            </div>

            <div class="owl-carousel testimonials-carousel">

            @foreach ($posts as $item)
                <div class="testimonial-item">
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        {{ Str::limit(strip_tags($item->content), 230) }}
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{ $item->picture ? asset('po-content/uploads/' . $item->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                        class="testimonial-img" alt="">
                    <h3>{{ $item->title }}</h3>
                </div>
            @endforeach

            </div>

        </div>
    </section><!-- End Testimonials Section -->
@endsection
