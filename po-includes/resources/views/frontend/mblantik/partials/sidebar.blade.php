<div class="card mt-5">
    <div class="card-body">
        <h5 class="card-title mb-2 font-weight-bold">Search</h5>
        <form action="{{ url('search') }}" method="GET">
            <div class="input-group mb-4">
                <input type="text" class="form-control" name="terms" placeholder="" aria-label=""
                    aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button"><i class="bx bx-search"></i></button>
                </div>
            </div>
        </form>
        <h5 class="card-title mb-2 font-weight-bold">Categories</h5>
        <ul class="list-unstyled mb-4">

            @foreach ($countCategoryPost ?? '' as $item)
                <a class="text-dark" href="{{ url('/category/' . $item->seotitle) }}">
                    <li><small>> {{ $item->title }} <span class="text-muted">({{ $item->posts_count }})</span></small>
                    </li>
                </a>
            @endforeach

        </ul>
        <h5 class="card-title mb-2 font-weight-bold">Recent Posts</h5>

        @foreach (latestPost(5) as $item)
            <div class="row mb-3">
                <div class="col-md-4 mt-1">
                    <img style="width: 100%"
                        src="{{ $item->picture ? asset('po-content/uploads/' . $item->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                        alt="Card image cap">
                </div>
                <div class="col-md-7  pl-1 p-0">
                    <a class="text-dark" href="{{ prettyUrl($item) }}">
                        <div class="mb-1 font-weight-bold">{{ $item->title }}</div>
                    </a>
                    <div class="text-muted font-italic">
                        <small>{{ date('M d, Y', strtotime($item->created_at)) }}</small>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>
