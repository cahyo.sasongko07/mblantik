@if(count($menu->children) > 0)
	<li class="drop-down">
		<a href="{{ url($menu->url) }}">{!! $menu->title !!}</a>
@else
	<li>
		@if($menu->target == 'none')
		<a href="{{ url($menu->url) }}">{!! $menu->title !!}</a>
		@else
		<a href="{{ $menu->url }}" target="_blank">{!! $menu->title !!}</a>
		@endif
@endif
	@if (count($menu->children) > 0)
		<ul>
			@foreach($menu->children as $menu)
				@include(getTheme('partials.submenu'), $menu)
			@endforeach
		</ul>
	@endif
</li>
