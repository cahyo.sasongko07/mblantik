<div class="media mt-3">
    <img class="mr-3" style="width: 50px; height: 50px; object-fit:cover;"
        src="{{ asset('po-admin/assets/img/avatar.jpg') }}" alt="Generic placeholder image">
    <div class="media-body">
        <div class="row col d-flex align-items-center">
            <h5 class="mt-0 mb-0 mr-3">{{ $comment->name }}</h5>
            <a href="javascript:void(0);" class="po-reply" id="{{ $comment->id }}">
                <span><strong>Reply</strong></span>
            </a>
        </div>
        <div class="text-muted font-italic mb-2">
            <small>{{ date('M d, Y', strtotime($comment->created_at)) }}</small>
        </div>
        <p>{{ strip_tags($comment->content) }}</p>

        @if (count($comment->children) > 0)
            @foreach ($comment->children as $comment)
                @include(getTheme('partials.subcomment'), $comment)
            @endforeach
        @endif

    </div>
</div>
