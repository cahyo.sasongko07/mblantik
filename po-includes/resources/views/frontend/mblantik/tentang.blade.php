@extends(getTheme('layouts.app'))
@section('content')
    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-1 mt-5">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-1.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-2 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-2.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-3 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-3.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-4 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-4.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-5 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-5.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-6 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-6.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-7 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-7.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-8 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-8.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-9 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-9.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-10 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-10.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-11 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-11.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-12 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-12.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-13 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-13.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-14 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-14.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services tentang-15 mt-3">
        <div class="container">
            <img src="{{ asset('po-content/uploads/tentang-15.png') }}" class="img-fluid" alt="">
        </div>
    </section><!-- End Services Section -->

@endsection

@push('script')
    <script type="text/javascript">
        !(function($) {
            "use strict";

            $("form.php-email-form").submit(function(e) {
                e.preventDefault();

                var f = $(this).find(".form-group"),
                    ferror = false,
                    emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

                f.children("input").each(function() {
                    // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr("data-rule");

                    if (rule !== undefined) {
                        var ierror = false; // error flag for current input
                        var pos = rule.indexOf(":", 0);
                        if (pos >= 0) {
                            var exp = rule.substr(pos + 1, rule.length);
                            rule = rule.substr(0, pos);
                        } else {
                            rule = rule.substr(pos + 1, rule.length);
                        }

                        switch (rule) {
                            case "required":
                                if (i.val() === "") {
                                    ferror = ierror = true;
                                }
                                break;

                            case "minlen":
                                if (i.val().length < parseInt(exp)) {
                                    ferror = ierror = true;
                                }
                                break;

                            case "email":
                                if (!emailExp.test(i.val())) {
                                    ferror = ierror = true;
                                }
                                break;

                            case "checked":
                                if (!i.is(":checked")) {
                                    ferror = ierror = true;
                                }
                                break;

                            case "regexp":
                                exp = new RegExp(exp);
                                if (!exp.test(i.val())) {
                                    ferror = ierror = true;
                                }
                                break;
                        }
                        i.next(".validate")
                            .html(
                                ierror ?
                                i.attr("data-msg") !== undefined ?
                                i.attr("data-msg") :
                                "wrong Input" :
                                ""
                            )
                            .show("blind");
                    }
                });
                f.children("textarea").each(function() {
                    // run all inputs

                    var i = $(this); // current input
                    var rule = i.attr("data-rule");

                    if (rule !== undefined) {
                        var ierror = false; // error flag for current input
                        var pos = rule.indexOf(":", 0);
                        if (pos >= 0) {
                            var exp = rule.substr(pos + 1, rule.length);
                            rule = rule.substr(0, pos);
                        } else {
                            rule = rule.substr(pos + 1, rule.length);
                        }

                        switch (rule) {
                            case "required":
                                if (i.val() === "") {
                                    ferror = ierror = true;
                                }
                                break;

                            case "minlen":
                                if (i.val().length < parseInt(exp)) {
                                    ferror = ierror = true;
                                }
                                break;
                        }
                        i.next(".validate")
                            .html(
                                ierror ?
                                i.attr("data-msg") != undefined ?
                                i.attr("data-msg") :
                                "wrong Input" :
                                ""
                            )
                            .show("blind");
                    }
                });
                if (ferror) return false;

                var this_form = $(this);
                var action = $(this).attr("action");

                if (!action) {
                    this_form.find(".loading").slideUp();
                    this_form
                        .find(".error-message")
                        .slideDown()
                        .html("The form action property is not set!");
                    return false;
                }

                this_form.find(".sent-message").slideUp();
                this_form.find(".error-message").slideUp();
                this_form.find(".loading").slideDown();

                if ($(this).data("recaptcha-site-key")) {
                    var recaptcha_site_key = $(this).data("recaptcha-site-key");
                    grecaptcha.ready(function() {
                        grecaptcha
                            .execute(recaptcha_site_key, {
                                action: "php_email_form_submit"
                            })
                            .then(function(token) {
                                php_email_form_submit(
                                    this_form,
                                    action,
                                    this_form.serialize() + "&recaptcha-response=" + token
                                );
                            });
                    });
                } else {
                    php_email_form_submit(this_form, action);
                }

                return true;
            });

            function php_email_form_submit(this_form, action) {
                $.ajax({
                        type: "POST",
                        url: action,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            'name': $('#name').val(),
                            'email': $('#email').val(),
                            'subject': $('#subject').val(),
                            'message': $('#message').val(),
                        },
                        timeout: 40000,
                    })
                    .done(function(msg) {
                        if (msg) {
                            this_form.find(".loading").slideUp();
                            this_form.find(".sent-message").slideDown();
                            this_form.find("input:not(input[type=submit]), textarea").val("");
                        } else {
                            this_form.find(".loading").slideUp();
                            if (!msg) {
                                msg =
                                    "Form submission failed and no error message returned from: " +
                                    action +
                                    "<br>";
                            }
                            this_form.find(".error-message").slideDown().html(msg);
                        }
                    })
                    .fail(function(data) {
                        console.log(data);
                        var error_msg = "Form submission failed!<br>";
                        if (data.statusText || data.status) {
                            error_msg += "Status:";
                            if (data.statusText) {
                                error_msg += " " + data.statusText;
                            }
                            if (data.status) {
                                error_msg += " " + data.status;
                            }
                            error_msg += "<br>";
                        }
                        if (data.responseText) {
                            error_msg += data.responseText;
                        }
                        this_form.find(".loading").slideUp();
                        this_form.find(".error-message").slideDown().html(error_msg);

                    });
            }
        })(jQuery);

    </script>
@endpush
