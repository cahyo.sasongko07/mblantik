@extends(getTheme('layouts.app'))
@section('content')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                {{-- <img src="{{ asset('po-content/frontend/mblantik/img/logo.png') }}"
                class="img-fluid" alt=""> --}}
                <h1>mBLANTIK</h1>
                <h2>Menjembatani semua orang yang ingin berbisnis sapi</h2>
                {{-- <div class="d-flex">
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video"
                            data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>
                    </div> --}}
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img">
                <img src="{{ asset('po-content/frontend/mblantik/img/8400.jpg') }}" class="img-fluid animated" alt="">
            </div>
        </div>
    </div>
</section><!-- End Hero -->

<!-- ======= About Section ======= -->
{{--<section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="{{ asset('po-content/frontend/mblantik/img/about.png') }}" class="img-fluid" alt="">
</div>
<div class="col-lg-6 pt-4 pt-lg-0 content">
    <h3>Tentang mBLANTIK</h3>
    <p class="font-italic">
        Merupakan produk perusahaan start-up yang menjembatani semua orang yang memiliki keinginan berbisnis sapi namun ada keterbatasan kemampuan finansial, pengetahuan peternakan, tidak ingin kotor & keterbatasan lahan.
    </p>
    <p>
        Digital Content :
    </p>
    <ul>
        <li><i class="icofont-circled-right"></i> Corporate => <a href="http://blantik.co.id">http://blantik.co.id</a> (under construction)</li>
        <li><i class="icofont-circled-right"></i> Produk => <a href="http://mblantik.id">http://mblantik.id</a> (under construction)</li>
        <li><i class="icofont-circled-right"></i> Play Store => mblantik (under development)</li>
    </ul>
    <a href="{{ url('category/tentang') }}" class="btn btn-primary btn-sm">More</a>
</div>
</div>
</div>
</section>--}}
<!-- End About -->

<section id="featured-services" class="featured-services">
    <div class="container">
        <h3 class="d-flex justify-content-center mb-4">Ada 3 (tiga) skema bisnis yang ditawarkan</h3>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="icon-box">
                    <div class="icon"><i class="icofont-bullhorn"></i></div>
                    <h4 class="title"><a href="">Re-Seller</a></h4>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-4 mt-md-0">
                <div class="icon-box">
                    <div class="icon"><i class="icofont-bucket"></i></div>
                    <h4 class="title"><a href="">Mitra Kandang</a></h4>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mt-4 mt-lg-0">
                <div class="icon-box">
                    <div class="icon"><i class="icofont-coins"></i></div>
                    <h4 class="title"><a href="">Usaha Bersama</a></h4>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- ======= Counts Section ======= -->
{{--<section id="counts" class="counts">
        <div class="container">

            <div class="row counters">

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">232</span>
                    <p>Clients</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">521</span>
                    <p>Projects</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">1,463</span>
                    <p>Hours Of Support</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">15</span>
                    <p>Hard Workers</p>
                </div>

            </div>

        </div>
    </section>--}}<!-- End Counts Section -->

<!-- ======= Services Section ======= -->
<section id="services" class="services section-bg">
    <div class="container">

        <div class="section-title">
            <span>Mitra Kandang mBlantik</span>
            <h2>Mitra Kandang mBlantik</h2>
        </div>

        <div class="row" style="text-align: center;">
            <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
                <div>
                    <h4>CV Agung Barokah – Cilodong (Kapasitas : 400 ekor)</h4>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
                <div>
                    <h4 style="margin-left: 50px;">Famous Farm – Purworejo (Konstruksi)</h4>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            @foreach ($gallerys as $item)
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="icon-box">
                    <div class="icon">
                        <div class="img-fluid mt-3" style="background: url('{{ getPicture($item->picture, '', $item->updated_by) }}') 50% 50% no-repeat; width: 250px; height: 200px;"></div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>


    </div>
</section><!-- End Services Section -->

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="section-title">
            <span>Jenis Sapi Potong</span>
            <h2>Jenis Sapi Potong</h2>
        </div>

        {{-- <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div> --}}

        <div class="row portfolio-container">

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_brahman.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Brahman</h4>
                    <p>Sapi Brahman</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_brahman.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Brahman"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_limosin.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Limosin</h4>
                    <p>Sapi Limosin</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_limosin.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Limosin"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_simental.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Simental</h4>
                    <p>Sapi Simental</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_simental.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Simental"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_pegon.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Pegon</h4>
                    <p>Sapi Pegon</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_pegon.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Pegon"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_brangus.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Brangus</h4>
                    <p>Sapi Brangus</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_brangus.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Brangus"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_bali.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Bali</h4>
                    <p>Sapi Bali</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_bali.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Bali"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                <img src="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_madura.jpg') }}" class="img-fluid" alt="" style="width: 400px; height: 250px;">
                <div class="portfolio-info">
                    <h4>Sapi Madura</h4>
                    <p>Sapi Madura</p>
                    <a href="{{ asset('po-content/frontend/mblantik/img/portfolio/sapi_madura.jpg') }}" data-gall="portfolioGallery" class="venobox preview-link" title="Sapi Madura"><i class="bx bx-plus"></i></a>
                    <a href="#" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>

        </div>

    </div>
</section><!-- End Portfolio Section -->

<!-- ======= Testimonials Section ======= -->
<section id="testimonials" class="testimonials section-bg">
    <div class="container">

        <div class="section-title">
            <span>Testimoni</span>
            <h2>Testimoni</h2>
            <!-- <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p> -->
        </div>

        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Selamat dan semoga semakin sukses dan berkembang usaha nya mas Agung, hasil nya pun barokah dan
                    bermanfaat bagi banyak orang. Saya tidak akan pernah bosan dan berharap dapat terus bekerjasama dan
                    maju bersama mas Agung. Aamin.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <img src="{{ asset('po-content/frontend/mblantik/img/testimonials/testimonials-6.jpg') }}" class="testimonial-img" alt="">
                <h3>Unang Satya Graha, Bandung</h3>
                <h4>Founder</h4>
            </div>

            <div class="testimonial-item">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Buat saya yang ga ngerti sama sekali tentang seluk beluk bisnis Sapi, dan ingin menginvestasikan
                    uang saya di bidang tersebut, saya sangat merekomendasikan bisnis penggemukan sapi sebagai Mitra
                    Investasi dari mBLANTIK.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <img src="{{ asset('po-content/frontend/mblantik/img/testimonials/testimonials-7.jpg') }}" class="testimonial-img" alt="">
                <h3>Ardi Susetyo (Kemayoran, Jakarta Pusat) </h3>
                <h4>Pemilik Sapi</h4>
            </div>

            <div class="testimonial-item">
                <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    Dengan menggandeng mBLANTIK sebagai partner, Famous Farm telah memberikan manfaat bagi masyarakat
                    sekitar sebagai terutama dalam manajemen kandang, manajemen pakan, perawatan sapi, dan pengolahan
                    limbah sapi.
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
                <img src="{{ asset('po-content/frontend/mblantik/img/testimonials/testimonials-8.jpg') }}" class="testimonial-img" alt="">
                <h3>Kim Fajrin Putu Hadi</h3>
                <h4>Owner dari Famous Farm – Mitra Kandang (Yogyakarta)</h4>
            </div>

        </div>

    </div>
</section><!-- End Testimonials Section -->

<!-- ======= Cta Section ======= -->
{{-- <section id="cta" class="cta">
        <div class="container">

            <div class="text-center">
                <h3>Call To Action</h3>
                <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                    laborum.</p>
                <a class="cta-btn" href="#">Call To Action</a>
            </div>

        </div>
    </section> --}}
<!-- End Cta Section -->

<!-- ======= Team Section ======= -->
{{-- <section id="team" class="team section-bg">
        <div class="container">

            <div class="section-title">
                <span>Team</span>
                <h2>Team</h2>
                <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="member">
                        <img src="{{ asset('po-content/frontend/mblantik/img/team/team-1.jpg') }}" alt="">
<h4>Walter White</h4>
<span>Chief Executive Officer</span>
<p>
    Magni qui quod omnis unde et eos fuga et exercitationem. Odio veritatis perspiciatis quaerat qui
    aut aut aut
</p>
<div class="social">
    <a href=""><i class="icofont-twitter"></i></a>
    <a href=""><i class="icofont-facebook"></i></a>
    <a href=""><i class="icofont-instagram"></i></a>
    <a href=""><i class="icofont-linkedin"></i></a>
</div>
</div>
</div>

<div class="col-lg-4 col-md-6 d-flex align-items-stretch">
    <div class="member">
        <img src="{{ asset('po-content/frontend/mblantik/img/team/team-2.jpg') }}" alt="">
        <h4>Sarah Jhinson</h4>
        <span>Product Manager</span>
        <p>
            Repellat fugiat adipisci nemo illum nesciunt voluptas repellendus. In architecto rerum rerum
            temporibus
        </p>
        <div class="social">
            <a href=""><i class="icofont-twitter"></i></a>
            <a href=""><i class="icofont-facebook"></i></a>
            <a href=""><i class="icofont-instagram"></i></a>
            <a href=""><i class="icofont-linkedin"></i></a>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-6 d-flex align-items-stretch">
    <div class="member">
        <img src="{{ asset('po-content/frontend/mblantik/img/team/team-3.jpg') }}" alt="">
        <h4>William Anderson</h4>
        <span>CTO</span>
        <p>
            Voluptas necessitatibus occaecati quia. Earum totam consequuntur qui porro et laborum toro des
            clara
        </p>
        <div class="social">
            <a href=""><i class="icofont-twitter"></i></a>
            <a href=""><i class="icofont-facebook"></i></a>
            <a href=""><i class="icofont-instagram"></i></a>
            <a href=""><i class="icofont-linkedin"></i></a>
        </div>
    </div>
</div>

</div>

</div>
</section> --}}
<!-- End Team Section -->

<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">
    <div class="container">
        <div class="section-title">
            <span>Kontak</span>
            <h2>Kontak</h2>
        </div>
        <div class="row">
            {{-- <img src="{{ asset('po-content/frontend/mblantik/img/kontak.jpg') }}"
            class="img-fluid" alt=""> --}}
            <div class="col-lg-5 d-flex align-items-stretch">
                <div class="info">
                    {{-- <div class="address">
                            <i class="icofont-google-map"></i>
                            <h4>Location:</h4>
                            <p>A108 Adam Street, New York, NY 535022</p>
                        </div>
                        <div class="email">
                            <i class="icofont-envelope"></i>
                            <h4>Email:</h4>
                            <p>info@example.com</p>
                        </div> --}}
                    <div class="phone">
                        <i class="icofont-phone"></i>
                        <h4>R. Buce Mulyana:</h4>
                        <p>0813-8679-4118</p>
                    </div>
                    {{-- <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                            frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
                        --}}
                </div>
            </div>
            <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
                <form action="{{ url('contact/send') }}" method="post" role="form" class="php-email-form">

                    @csrf
                    @method('post')

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Your Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validate"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Your Email</label>
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject" value="{{ old('subject') }}" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <label for="name">Message</label>
                        <textarea class="form-control" name="message" id="message" rows="10" data-rule="required" data-msg="Please write something for us">{{ old('message') }}</textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>

        </div>

    </div>
</section>
<!-- End Contact Section -->

@endsection

@push('script')
<script type="text/javascript">
    !(function($) {
        "use strict";

        $("form.php-email-form").submit(function(e) {
            e.preventDefault();

            var f = $(this).find(".form-group"),
                ferror = false,
                emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

            f.children("input").each(function() {
                // run all inputs

                var i = $(this); // current input
                var rule = i.attr("data-rule");

                if (rule !== undefined) {
                    var ierror = false; // error flag for current input
                    var pos = rule.indexOf(":", 0);
                    if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                    } else {
                        rule = rule.substr(pos + 1, rule.length);
                    }

                    switch (rule) {
                        case "required":
                            if (i.val() === "") {
                                ferror = ierror = true;
                            }
                            break;

                        case "minlen":
                            if (i.val().length < parseInt(exp)) {
                                ferror = ierror = true;
                            }
                            break;

                        case "email":
                            if (!emailExp.test(i.val())) {
                                ferror = ierror = true;
                            }
                            break;

                        case "checked":
                            if (!i.is(":checked")) {
                                ferror = ierror = true;
                            }
                            break;

                        case "regexp":
                            exp = new RegExp(exp);
                            if (!exp.test(i.val())) {
                                ferror = ierror = true;
                            }
                            break;
                    }
                    i.next(".validate")
                        .html(
                            ierror ?
                            i.attr("data-msg") !== undefined ?
                            i.attr("data-msg") :
                            "wrong Input" :
                            ""
                        )
                        .show("blind");
                }
            });
            f.children("textarea").each(function() {
                // run all inputs

                var i = $(this); // current input
                var rule = i.attr("data-rule");

                if (rule !== undefined) {
                    var ierror = false; // error flag for current input
                    var pos = rule.indexOf(":", 0);
                    if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                    } else {
                        rule = rule.substr(pos + 1, rule.length);
                    }

                    switch (rule) {
                        case "required":
                            if (i.val() === "") {
                                ferror = ierror = true;
                            }
                            break;

                        case "minlen":
                            if (i.val().length < parseInt(exp)) {
                                ferror = ierror = true;
                            }
                            break;
                    }
                    i.next(".validate")
                        .html(
                            ierror ?
                            i.attr("data-msg") != undefined ?
                            i.attr("data-msg") :
                            "wrong Input" :
                            ""
                        )
                        .show("blind");
                }
            });
            if (ferror) return false;

            var this_form = $(this);
            var action = $(this).attr("action");

            if (!action) {
                this_form.find(".loading").slideUp();
                this_form
                    .find(".error-message")
                    .slideDown()
                    .html("The form action property is not set!");
                return false;
            }

            this_form.find(".sent-message").slideUp();
            this_form.find(".error-message").slideUp();
            this_form.find(".loading").slideDown();

            if ($(this).data("recaptcha-site-key")) {
                var recaptcha_site_key = $(this).data("recaptcha-site-key");
                grecaptcha.ready(function() {
                    grecaptcha
                        .execute(recaptcha_site_key, {
                            action: "php_email_form_submit"
                        })
                        .then(function(token) {
                            php_email_form_submit(
                                this_form,
                                action,
                                this_form.serialize() + "&recaptcha-response=" + token
                            );
                        });
                });
            } else {
                php_email_form_submit(this_form, action);
            }

            return true;
        });

        function php_email_form_submit(this_form, action) {
            $.ajax({
                    type: "POST",
                    url: action,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'name': $('#name').val(),
                        'email': $('#email').val(),
                        'subject': $('#subject').val(),
                        'message': $('#message').val(),
                    },
                    timeout: 40000,
                })
                .done(function(msg) {
                    if (msg) {
                        this_form.find(".loading").slideUp();
                        this_form.find(".sent-message").slideDown();
                        this_form.find("input:not(input[type=submit]), textarea").val("");
                    } else {
                        this_form.find(".loading").slideUp();
                        if (!msg) {
                            msg =
                                "Form submission failed and no error message returned from: " +
                                action +
                                "<br>";
                        }
                        this_form.find(".error-message").slideDown().html(msg);
                    }
                })
                .fail(function(data) {
                    console.log(data);
                    var error_msg = "Form submission failed!<br>";
                    if (data.statusText || data.status) {
                        error_msg += "Status:";
                        if (data.statusText) {
                            error_msg += " " + data.statusText;
                        }
                        if (data.status) {
                            error_msg += " " + data.status;
                        }
                        error_msg += "<br>";
                    }
                    if (data.responseText) {
                        error_msg += data.responseText;
                    }
                    this_form.find(".loading").slideUp();
                    this_form.find(".error-message").slideDown().html(error_msg);

                });
        }
    })(jQuery);
</script>
@endpush