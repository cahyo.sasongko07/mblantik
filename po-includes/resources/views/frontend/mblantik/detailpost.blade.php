@extends(getTheme('layouts.app'))

@section('content')
    <!-- ======= Hero Section ======= -->
    {{-- <section id="hero" class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Elegant and creative solutions</h1>
                    <h2>We are team of talanted designers making websites with Bootstrap</h2>
                    <div class="d-flex">
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video"
                            data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ asset('po-content/frontend/mblantik/img/8400.jpg') }}" class="img-fluid animated" alt="">
                </div>
            </div>
        </div>
    </section> --}}
    <!-- End Hero -->

    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card mt-5">
                        <img class="card-img-top"
                            src="{{ $post->picture ? asset('po-content/uploads/' . $post->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                            alt="Card image cap">
                        <div class="card-body">
                            <a class="text-dark" href="{{ prettyUrl($post) }}">
                                <h5 class="card-title font-weight-bold">{{ $post->title }}</h5>
                            </a>
                            <ul class="list-inline text-muted">
                                <li class="list-inline-item"><small><i
                                            class="bx bxs-user mr-1"></i>{{ $post->name }}</small>
                                </li>
                                <li class="list-inline-item">
                                    <small>
                                        <i class="bx bx-time mr-1"></i>{{ date('M d, Y', strtotime($post->created_at)) }}
                                    </small>
                                </li>
                                <li class="list-inline-item">
                                    <small>
                                        <i class="bx bxs-comment-detail mr-1"></i>{{ $post->comments_count }} Comment
                                    </small>
                                </li>
                            </ul>
                            <div class="card-text">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>

                    @if (getSetting('comment') == 'Y')
                        <div class="mt-4">
                            <h3>{{ $post->comments_count }} Comments</h3>

                            @if ($post->comments_count > 0)

                                @each(getTheme('partials.comment'), getComments($post->id, 5), 'comment',

                                getTheme('partials.comment'))
                            @else
                                <p class="text-center mt-2">There are no comments yet</p>
                            @endif

                        </div>
                    @endif



                    <div class="card mt-4" id="comment-form">
                        <form action="{{ url('comment/send/' . $post->seotitle) }}" method="post">

                            @csrf
                            @method('post')

                            <div class="card-body">
                                <h5 class="card-title mb-0 font-weight-bold">Leave a Comment</h5>
                                <div class="text-muted mb-4">
                                    <small>Your email address will not be published. Required fields are marked *</small>
                                </div>

                                @if (session()->has('flash_message'))
                                    <div class="alert alert-success">{{ session()->get('flash_message') }}</div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger" id="alert-error">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <input type="hidden" name="parent" id="parent"
                                    value="{{ old('parent') == null ? 0 : old('parent') }}" />
                                <input type="hidden" name="post_id" id="post_id" value="{{ $post->id }}" />
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" id="name" name="name"
                                            value="{{ old('name') }}" placeholder="Your Name *">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" class="form-control" id="email" name="email"
                                            value="{{ old('email') }}" placeholder="Your Email *">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="content" name="content" placeholder="Your Comment *"
                                        rows="3">{{ old('content') }}</textarea>
                                </div>
                                <button type="submit" class="btn btn-secondary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    @include(getTheme('partials.sidebar'))
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script type="text/javascript">
        $(function() {
            $('#comment-form #parent').val(0)
            $('.po-reply').on('click', function() {
                var id = $(this).attr('id');
                var parentId = $('#comment-form #parent').val()

                if (parentId == 0 || id != parentId) {

                    $('[id=icon-reply]').remove()
                    $(this).prepend('<i class="icofont-reply" id="icon-reply"></i>')
                    $('#comment-form #parent').val(id);

                    $('html, body').animate({
                        scrollTop: $("#comment-form").offset().top - 150
                    }, 1000);

                } else if (id == parentId) {
                    $('#icon-reply', this).remove()
                    $('#comment-form #parent').val(0);
                }

            });
        });

    </script>
@endpush
