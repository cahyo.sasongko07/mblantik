@extends(getTheme('layouts.app'))

@section('content')
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if ($posts->total() > 0)
                        @foreach ($posts as $item)
                            <div class="card mt-5">
                                <img class="card-img-top"
                                    src="{{ $item->picture ? asset('po-content/uploads/' . $item->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <a class="text-dark" href="{{ prettyUrl($item) }}">
                                        <h5 class="card-title font-weight-bold">{{ $item->title }}</h5>
                                    </a>
                                    <ul class="list-inline text-muted">
                                        <li class="list-inline-item"><small><i
                                                    class="bx bxs-user mr-1"></i>{{ $item->name }}</small>
                                        </li>
                                        <li class="list-inline-item">
                                            <small>
                                                <i
                                                    class="bx bx-time mr-1"></i>{{ date('M d, Y', strtotime($item->created_at)) }}
                                            </small>
                                        </li>
                                        <li class="list-inline-item">
                                            <small>
                                                <i class="bx bxs-comment-detail mr-1"></i>{{ $item->comments_count }}
                                                Comment
                                            </small>
                                        </li>
                                    </ul>
                                    <div class="card-text">
                                        {{ Str::limit(strip_tags($item->content), 200) }}
                                    </div>
                                    <div class="mt-3 d-flex justify-content-end">
                                        <a href="{{ prettyUrl($item) }}" class="btn btn-primary btn-sm">Read More</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                    <div class="typography-content text-center mt-5">
                        <h1>404</h1>
                        <h3>Search Data Not Found</h3>
                        <p style="margin:30px auto;">Unfortunately the content you’re looking for isn’t here. There may be a misspelling in your web address or you may have clicked a link for content that no longer exists. Perhaps you would be interested in our most recent articles.</p>
                    </div>
                    @endif
                    <nav class="mt-5" aria-label="Page navigation example">
                        {{ $posts->links() }}
                    </nav>
                </div>
                <div class="col-md-4">
                    @include(getTheme('partials.sidebar'))
                </div>

            </div>
        </div>
    </section>
@endsection
