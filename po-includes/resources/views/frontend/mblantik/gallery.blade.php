@extends(getTheme('layouts.app'))

@section('content')
    <section id="album" class="portfolio mt-5">
        <div class="container">

            @if (isset($album))
                <div class="section-title">
                    <span>{{ $album->title }} ({{ $gallerys->total() }})</span>
                    <h2>{{ $album->title }} ({{ $gallerys->total() }})</h2>
                </div>
            @else
                <div class="section-title">
                    <span>All Album</span>
                    <h2>All Album</h2>
                </div>
            @endif

            <div class="row portfolio-container" style="position: relative; height: 1080.35px;">
                @if (isset($album))
                    @foreach ($gallerys as $item)
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app"
                            style="position: absolute; left: 0px; top: 0px;">
                            <img src="{{ getPicture($item->picture, 'medium', $item->updated_by) }}" style="width: 100%"
                                class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>{{ $item->title }}</h4>
                                <p>{{ $item->atitle }}</p>
                                <a href="{{ getPicture($item->picture, 'medium', $item->updated_by) }}"
                                    data-gall="portfolioGallery" class="venobox preview-link details-link" title="{{ $item->title }}"><i
                                        class="bx bx-image"></i></a>

                            </div>
                        </div>
                    @endforeach
                @else
                    @foreach ($gallerys as $item)
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app"
                            style="position: absolute; left: 0px; top: 0px;">
                            <img src="{{ getPicture($item->gallerys[0]->picture, 'medium', $item->gallerys[0]->updated_by) }}" style="width: 100%"
                                class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4 class="mr-5 m-0">{{ $item->title }} ({{ count($item->gallerys) }})</h4>
                                <a href="{{ url('album/' . $item->seotitle) }}" class="details-link" title="More"><i
                                        class="bx bx-link"></i></a>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
@endsection
