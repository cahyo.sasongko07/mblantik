@extends(getTheme('layouts.app'))

@section('content')
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="{{!url('pages/tentang') ? 'col-md-8' : 'col-md-12'}}">
                    <div class="card mt-5">
                        <img class="card-img-top"
                            src="{{ $pages->picture ? asset('po-content/uploads/' . $pages->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                            alt="Card image cap">
                        <div class="card-body">
                            <a class="text-dark" href="{{ prettyUrl($pages) }}">
                                <h5 class="card-title font-weight-bold">{{ $pages->title }}</h5>
                            </a>
                            <ul class="list-inline text-muted">
                                <li class="list-inline-item"><small><i
                                            class="bx bxs-user mr-1"></i>{{ $pages->name }}</small>
                                </li>
                                <li class="list-inline-item">
                                    <small>
                                        <i class="bx bx-time mr-1"></i>{{ date('M d, Y', strtotime($pages->created_at)) }}
                                    </small>
                                </li>
                            </ul>
                            <div class="card-text">
                                {!! $pages->content !!}
                            </div>
                        </div>
                    </div>
                </div>
                @if (!url('pages/tentang'))
                    <div class="col-md-4">
                        @include(getTheme('partials.sidebar'))
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
