@extends(getTheme('layouts.app'))

@section('content')
    <!-- ======= Hero Section ======= -->
    {{-- <section id="hero" class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>Elegant and creative solutions</h1>
                    <h2>We are team of talanted designers making websites with Bootstrap</h2>
                    <div class="d-flex">
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox btn-watch-video"
                            data-vbtype="video" data-autoplay="true"> Watch Video <i class="icofont-play-alt-2"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ asset('po-content/frontend/mblantik/img/8400.jpg') }}" class="img-fluid animated" alt="">
                </div>
            </div>
        </div>
    </section> --}}
    <!-- End Hero -->

    <section id="blog">
        <div class="container">
            <div class="row">
                @if ($posts->total() > 0)
                    <div class="col-md-8">
                        @foreach ($posts as $item)
                            <div class="card mt-5">
                                <img class="card-img-top"
                                    src="{{ $item->picture ? asset('po-content/uploads/' . $item->picture) : asset('po-content/frontend/mblantik/img/no-image.jpg') }}"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <a class="text-dark" href="{{ prettyUrl($item) }}">
                                        <h5 class="card-title font-weight-bold">{{ $item->title }}</h5>
                                    </a>
                                    <ul class="list-inline text-muted">
                                        <li class="list-inline-item"><small><i
                                                    class="bx bxs-user mr-1"></i>{{ $item->name }}</small>
                                        </li>
                                        <li class="list-inline-item">
                                            <small>
                                                <i
                                                    class="bx bx-time mr-1"></i>{{ date('M d, Y', strtotime($item->created_at)) }}
                                            </small>
                                        </li>
                                        <li class="list-inline-item">
                                            <small>
                                                <i class="bx bxs-comment-detail mr-1"></i>{{ $item->comments_count }}
                                                Comment
                                            </small>
                                        </li>
                                    </ul>
                                    <div class="card-text">
                                        {{ Str::limit(strip_tags($item->content), 200) }}
                                    </div>
                                    <div class="mt-3 d-flex justify-content-end">
                                        <a href="{{ prettyUrl($item) }}" class="btn btn-primary btn-sm">Read More</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <nav class="mt-5" aria-label="Page navigation example">
                            {{ $posts->links() }}
                            {{-- <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul> --}}
                        </nav>
                    </div>
                    <div class="col-md-4">
                        @include(getTheme('partials.sidebar'))
                    </div>
                @else
                    <div class="col-md-12 mt-5">
                        <img class="img-fluid w-50 mx-auto d-block"
                        src="{{ asset('po-content/frontend/mblantik/img/404postDataNotFound.jpg') }}"
                        class="img-fluid animated" alt="">
                        <h4 class="text-center mt-3 font-weight-bold">NO POSTS</h4>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
